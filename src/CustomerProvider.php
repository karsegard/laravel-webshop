<?php

namespace KDA\Webshop;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use KDA\Laravel\PackageServiceProvider;


class CustomerProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasHelper;

    protected $routes = [
        '/kda/webshop/customer.auth.php'
    ];
   
    

    protected $configs = [
        'kda/webshop.php'=> 'kda.webshop'
    ];

    protected $registerViews = 'kda/webshop';
    protected $publishViewsTo = 'vendor/kda/webshop';
     
 
    protected function packageBaseDir () {
        return dirname(__DIR__,1);
    }

    

    public function boot (){
        $this->loadHelper('auth.php');
        
        parent::boot();
    }
   
}

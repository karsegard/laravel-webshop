# Webshop

## Customers Auth

    sail artisan vendor:publish --provider="KDA\Webshop\CustomerProvider" --tag="config" --force

### Install

Configure Laravel auth guard
To enable auth, you have to add config/auth.php

    'guards' => [
        ...
        'customers'=> [
            'driver' => 'session',
            'provider' => 'users' // <- change this if  you need another model
        ]
        ...
    ],

If you need to use another model

    'providers' => [
        ...
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\Customer::class,
        ],
        ...

    ],

in your model (the default one or the custom one), you have to declare the guard

    class User extends Authenticatable
    {

        protected $guard_name = 'customers';



## Orders

## Products

## Basket
